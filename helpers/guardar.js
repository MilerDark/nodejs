const fs = require('fs');

//La carpeta debe existir
const archivo ='./database/data.json';

const guardarDB = (data) => {
  fs.writeFileSync(archivo, JSON.stringify(data));
};


const leerDB = () => {
  //existsSync verifica si la ruta de la carpeta existe
  if(!fs.existsSync(archivo)){
    return null;
  }

  //Lee el archivo
  const info = fs.readFileSync(archivo, {encoding: 'utf-8'});
  //Convierte una cadena de JSON a un JSON
  const data = JSON.parse(info);

  return data;
};

module.exports = {
  guardarDB,
  leerDB,

}