const Persona = require("./persona")
class Personas {

  constructor() {
    this._listado = [];
  }

  crearPersona(persona = {}) {
    this._listado.push(persona);

    return this._listado;
  }

  get datosArr() {
    const datos = [];

    Object.keys(this._listado).forEach(key => {
      const persona = this._listado[key];
      datos.push(persona);
    })
    return datos;
  }
  cargarFromArray(personas = []) {
    
    personas.forEach(persona => {
      this._listado[persona.id] = persona;
    });
  }

  borrarTarea(id = ''){
    if(this._listado[id]){
      delete this._listado[id];
    }
  }
}

module.exports = Personas;